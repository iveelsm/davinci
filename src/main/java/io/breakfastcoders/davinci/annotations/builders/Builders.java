package io.breakfastcoders.davinci.annotations.builders;

import io.breakfastcoders.davinci.annotations.SourceType;
import io.breakfastcoders.davinci.error.UnsupportedTypeException;
import org.jetbrains.annotations.NotNull;

public enum Builders {
  JSON(new JsonDavinciClassBuilder()),
  YAML(new YamlDavinciClassBuilder());

  private final ClassBuilder builder;

  Builders(ClassBuilder builder) {
    this.builder = builder;
  }

  public static ClassBuilder getBuilderForSourceType(@NotNull SourceType type) throws UnsupportedTypeException {
    switch (type) {
      case JSON:
        return JSON.builder;
      case YAML:
        return YAML.builder;
      default:
        throw new UnsupportedTypeException("Unknown parameter was passed in");
    }
  }
}
