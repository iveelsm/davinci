package io.breakfastcoders.davinci.error;


/**
 * An extension of base level {@link Exception exception} for type support.
 * This exception will be invoked in instances where the user is attempting to use a
 * {@link io.breakfastcoders.davinci.annotations.SourceType type} that does not exist.
 * Or more likely, in cases where the user is trying to use an unsupported type.
 */
public class UnsupportedTypeException extends Exception {
  public UnsupportedTypeException(String message) {
    super(message);
  }
}
