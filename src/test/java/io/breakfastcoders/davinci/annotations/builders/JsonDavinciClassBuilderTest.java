package io.breakfastcoders.davinci.annotations.builders;

import io.breakfastcoders.davinci.serialization.strategy.Strategy;
import io.breakfastcoders.davinci.serialization.strategy.defaults.JacksonSnakeCaseStrategy;
import java.io.IOException;
import org.junit.Test;

import static io.breakfastcoders.davinci.utils.TestUtilities.getResourceString;
import static org.assertj.core.api.Assertions.assertThat;

public class JsonDavinciClassBuilderTest {
    private JsonDavinciClassBuilder builder = new JsonDavinciClassBuilder();

    @Test
    public void shouldBuildClassProperly() throws IOException {
        Class<? extends Strategy<?>> strategy = JacksonSnakeCaseStrategy.class;
        String className = "com.ibotta.test.Test";
        String classDefinition = builder.build(className, strategy);
        assertThat(classDefinition).isEqualTo(getResourceString(
                this.getClass().getClassLoader(),
                "class/TestClass"));
    }
}
