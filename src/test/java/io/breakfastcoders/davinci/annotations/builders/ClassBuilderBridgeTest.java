package io.breakfastcoders.davinci.annotations.builders;

import io.breakfastcoders.davinci.annotations.SourceType;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ClassBuilderBridgeTest {
    @Test
    public void shouldGetJsonBuilder() throws Exception {
        ClassBuilder builder = ClassBuilderBridge.getBuilder(SourceType.JSON);
        assertThat(builder).isNotNull();
        assertThat(builder).isInstanceOf(JsonDavinciClassBuilder.class);
    }
}
