package io.breakfastcoders.davinci.annotations;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AnnotationHelpersTest {

    @Test
    public void shouldGetPackageName() {
        String className = this.getClass().getName();
        String packageName = AnnotationHelpers.getPackage(className).toString();
        assertThat(packageName).isEqualTo(this.getClass().getPackage().getName());
    }

    @Test
    public void shouldGetRelativeName() {
        String className = this.getClass().getName();
        String relativeClassName = AnnotationHelpers.getRelativeName(className).toString();
        assertThat(relativeClassName).isEqualTo(this.getClass().getSimpleName());
    }
}
