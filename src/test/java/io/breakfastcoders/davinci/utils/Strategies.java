package io.breakfastcoders.davinci.utils;

import io.breakfastcoders.davinci.serialization.strategy.Strategy;
import io.breakfastcoders.davinci.serialization.strategy.defaults.JacksonCamelCaseStrategy;
import io.breakfastcoders.davinci.serialization.strategy.defaults.JacksonKebabCaseStrategy;
import io.breakfastcoders.davinci.serialization.strategy.defaults.JacksonPascalCaseStrategy;
import io.breakfastcoders.davinci.serialization.strategy.defaults.JacksonSnakeCaseStrategy;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Strategies {
    public static final Random r = new Random();

    public static List<Strategy> getStrategies() {
        List<Strategy> result = new ArrayList<>();
        result.add(new JacksonKebabCaseStrategy());
        result.add(new JacksonPascalCaseStrategy());
        result.add(new JacksonCamelCaseStrategy());
        result.add(new JacksonSnakeCaseStrategy());
        return result;
    }

    public static Strategy getRandomStrategy() {
        return getStrategies().get(r.nextInt(4));
    }
}
